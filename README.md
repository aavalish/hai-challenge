# Hai Challenge

## Install
- `git clone https://bitbucket.org/aavalish/hai-challenge`
- `cd hai-challenge`
- `yarn`

## Develop
- `yarn start`

## Build
- `yarn build`
