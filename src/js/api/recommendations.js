/*

  Recommendations API
  ~~~~~~~~~~~~~~~~~~~

*/

import axios from 'axios';
import { API_BASE_URL } from './config';

export async function getRecommendations(seen, userId, amt) {
  var params = new URLSearchParams();
  params.append('amt', amt);
  seen.map((item) => params.append('seen', item.id))
  return axios({
    method: 'get',
    url: `${ API_BASE_URL }/users/${ userId }/items`,
    params,
  });
};
