/*

  Hai Carousel Component
  ~~~~~~~~~~~~~~~~~~~~~~

*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './CarouselStyles.scss';

class Carousel extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  renderDots() {
    const { currentSlide, maxItems, itemsToShow } = this.props;
    return (
      <div className='hai-carousel-dots'>
        { [...Array(maxItems / itemsToShow)].map((e, i) => {
            return (
              <div
                key={ i }
                className={ `hai-carousel-dot ${ currentSlide === i ? 'is-active' : null }` }>
              </div>
            )
          })
        }
      </div>
    )
  }

  renderItems() {
    const { items, itemsLiked, currentSlide, itemsToShow } = this.props;
    const activeItems = [...items].slice(itemsToShow * currentSlide, itemsToShow + itemsToShow * currentSlide);
    return (
      <div className='hai-carousel-items'>
        { activeItems.map((item) => {
          return (
            <div key={ item.id } className='hai-carousel-item'>
              <div
                className='hai-carousel-item-cover'
                style={ { backgroundImage: `url(${ item.image })` }}>
                <div
                  className='hai-carousel-item-cover-heart'
                  onClick={ () => this.props.handleLikedItem(item) }>
                  <i className='material-icons'>
                    { itemsLiked.includes(item) ? 'favorite' : 'favorite_border' }
                  </i>
                </div>
              </div>
              <div className='hai-carousel-item-details'>
                <h3 className='hai-carousel-item-title'>
                  { item.name }
                </h3>
                <h5 className='hai-carousel-item-context'>
                  { item.definingInfo }
                </h5>
              </div>
            </div>
          )
        })}
      </div>
    )
  }

  render() {
    const { title } = this.props;
    return (
      <div className='hai-carousel'>
        <div
          className='hai-carousel-arrow'
          onClick={ () => this.props.handlePressedPrevious() }>
          <i className='hai-carousel-arrow-icon material-icons'>chevron_left</i>
        </div>
        <div className='hai-carousel-body'>
          <div className='hai-carousel-header'>
            <h1 className='hai-carousel-title'>{ title }</h1>
            { this.renderDots() }
          </div>
          { this.renderItems() }
        </div>
        <div
          className='hai-carousel-arrow'
          onClick={ () => this.props.handlePressedNext() }>
          <i className='hai-carousel-arrow-icon material-icons'>chevron_right</i>
        </div>
      </div>
    )
  }

}

Carousel.propTypes = {
  handlePressedNext: PropTypes.func,
  handlePressedPrevious: PropTypes.func,
  handleLikedItem: PropTypes.func,
  title: PropTypes.string,
  items: PropTypes.array,
  itemsLiked: PropTypes.array,
};

export default Carousel;
