import React, { Component } from 'react';
import { Carousel } from '../../components';
import './RecommendationsStyles.scss';
import { getRecommendations } from '../../api/recommendations';

class Recommendations extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userId: 0,
      items: [],
      itemsSeen: [],
      itemsLiked: [],
      currentSlide: 0,
      itemsToShow: 4,
      maxItems: 16,
    };
    this.handlePressedPrevious = this.handlePressedPrevious.bind(this);
    this.handlePressedNext = this.handlePressedNext.bind(this);
    this.handleLikedItem = this.handleLikedItem.bind(this);
    this.handleNavigatedToSlide = this.handleNavigatedToSlide.bind(this);
  }

  async componentWillMount() {
    const { userId, itemsSeen, maxItems, itemsToShow, currentSlide } = this.state;
    try {
      const recommendations = await getRecommendations(itemsSeen, userId, maxItems);
      const newlySeenItems = [...recommendations.data.items].splice(itemsToShow * currentSlide, itemsToShow + itemsToShow * currentSlide);
      this.setState({
        items: recommendations.data.items,
        itemsSeen: [...itemsSeen, ...newlySeenItems],
      })
    }
    catch (error) {
      console.error(error)
    }
  }

  handleNavigatedToSlide(slide) {
    this.setState({
      currentSlide: slide,
    })
  }

  handlePressedPrevious() {
    const { currentSlide } = this.state;
    this.setState({
      currentSlide: currentSlide === 0 ? 0 : currentSlide - 1,
    })
  }

  async handlePressedNext() {
    const { userId, items, itemsSeen, currentSlide, itemsToShow} = this.state;
    this.setState({
      currentSlide: currentSlide === 3 ? 3 : currentSlide + 1,
    })
    if (items.length < 16) {
      try {
        const newRecommendations = await getRecommendations(this.state.itemsSeen, userId, itemsToShow);
        const newlySeenItems = [...items].splice(itemsToShow * this.state.currentSlide, itemsToShow + itemsToShow * this.state.currentSlide);
        this.setState({
          items: newRecommendations.data.items,
          itemsSeen: [...itemsSeen, ...newlySeenItems],
          currentSlide: this.state.currentSlide === 3 ? 3 : this.state.currentSlide + 1,
        })
        console.log(this.state.itemsSeen);
      }
      catch (error) {
        console.error(error)
      }
    }
  }

  async handleLikedItem(item) {
    const { userId, items, itemsLiked, itemsSeen } = this.state;
    const itemIndex = items.findIndex(i => i.id === item.id);
    try {
      const newRecommendations = await getRecommendations([...itemsSeen, item], userId, 1);
      this.setState({
        items: [...items, newRecommendations.data.items[0]],
        itemsSeen: [...itemsSeen, item, newRecommendations.data.items[0]],
        itemsLiked: [...itemsLiked, item],
      })
    }
    catch (error) {
      console.error(error)
    }
    setTimeout(() => {
      this.setState({
        items: [...this.state.items].slice(0, itemIndex).concat([...this.state.items].slice(itemIndex + 1, this.state.items.length)),
      })
    }, 300);
  }

  render() {
    const { items, itemsLiked, currentSlide, itemsToShow, maxItems } = this.state;
    return (
      <div className='hai-recommendations'>
        <Carousel
          title='Top recommendations for you'
          items={ items }
          itemsLiked={ itemsLiked }
          currentSlide={ currentSlide }
          itemsToShow={ itemsToShow }
          maxItems={ maxItems }
          handlePressedPrevious={ this.handlePressedPrevious }
          handlePressedNext={ this.handlePressedNext }
          handleLikedItem={ this.handleLikedItem } />
      </div>
    )
  }

}

export default Recommendations;
